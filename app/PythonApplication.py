import serial
#Список скоростей передачи информации
speeds = ['1200','2400', '4800', '9600', '19200', '38400', '57600', '115200']
#Функция определения списка последовательных портов в Windows
def getPorts():
 ports = ['COM%s' % (i + 1) for i in range(256)]
 result = []

 for port in ports:
     try:
        s = serial.Serial(port)
        s.close()
        result.append(port)
     except (OSError, serial.SerialException):
        pass
 return result
def shift(value, num):
    return int(bool(value & (1 << num)) == True)
def main():
 #Определяем порты в системе
 ports = getPorts()
 print(ports)

 #Создаем объект для класса serial (список параметров для Ардуино по умолчанию)
 s = serial.Serial(ports[0], speeds[3], serial.EIGHTBITS, serial.PARITY_NONE, serial.STOPBITS_ONE, timeout
= 0, dsrdtr = True )
 print("Connected to: " + s.portstr)
 number = 0
 while number < 5:
    while s.in_waiting: #Проверяем наличие сообщения
        line = s.readline();
        values = line.split('-');
        temperature = int(values[0]);
        check = int(values[1]);

        b8 = shift(temperature, 0);
        b7 = shift(temperature, 1);
        b6 = shift(temperature, 2);
        b5 = shift(temperature, 3);
        b4 = shift(temperature, 4);
        b3 = shift(temperature, 5);
        b2 = shift(temperature, 6);
        b1 = shift(temperature, 7);

        a1 = shift(check, 3);
        a2 = shift(check, 2);
        a3 = shift(check, 1);
        a4 = shift(check, 0);

        s1 = b3 ^ b4 ^ b6 ^ b7 ^ a1;
        s2 = b3 ^ b5 ^ b6 ^ b8 ^ a2;
        s3 = b4 ^ b5 ^ b6 ^ a3;
        s4 = b7 ^ b8 ^ a4;
        if not (s1 & s2 & s3 & s4):
            print("Success")
        else:
            print("Not Successful")
        #data = s.read(4) #Считываем 4 байта данных ()
        print(number, int(data))
        number += 1
 s.close()
if __name__ == '__main__':
 main()
