def shift(value, num):
    return int(bool(value & (1 << num)) == True)
line = "51-10"
values = line.split("-");
print(values[0])
temperature = int(values[0]);
check = int(values[1]);

b8 = shift(temperature, 0);
b7 = shift(temperature, 1);
b6 = shift(temperature, 2);
b5 = shift(temperature, 3);
b4 = shift(temperature, 4);
b3 = shift(temperature, 5);
b2 = shift(temperature, 6);
b1 = shift(temperature, 7);

a1 = shift(check, 3);
a2 = shift(check, 2);
a3 = shift(check, 1);
a4 = shift(check, 0);

s1 = b3 ^ b4 ^ b6 ^ b7 ^ a1;
s2 = b3 ^ b5 ^ b6 ^ b8 ^ a2;
s3 = b4 ^ b5 ^ b6 ^ a3;
s4 = b7 ^ b8 ^ a4;
print(s1,s2,s3,s4)

