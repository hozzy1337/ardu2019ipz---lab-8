import serial
#Список скоростей передачи информации
speeds = ['1200','2400', '4800', '9600', '19200', '38400', '57600', '115200']
#Функция определения списка последовательных портов в Windows
def getPorts():
 ports = ['COM%s' % (i + 1) for i in range(256)]
 result = []

 for port in ports:
     try:
        s = serial.Serial(port)
        s.close()
        result.append(port)
     except (OSError, serial.SerialException):
        pass
 return result
def main():
 #Определяем порты в системе
 ports = getPorts()
 print(ports)

 #Создаем объект для класса serial (список параметров для Ардуино по умолчанию)
 s = serial.Serial(ports[1], speeds[3], serial.EIGHTBITS, serial.PARITY_NONE, serial.STOPBITS_ONE, timeout
= 0, dsrdtr = True )
 print("Connected to: " + s.portstr)
 number = 0
 while number < 5:
    while s.in_waiting: #Проверяем наличие сообщения
        data = s.read(4) #Считываем 4 байта данных ()
        print(number, int(data))
        number += 1
 s.close()
if __name__ == '__main__':
 main()
