int sensorPin = A5;
int sensorValue = 0;
float Q;
float Udon = 5000;
float N = 1024; // maybe 1023?
float Rmax = 10000;
float Uout;
float Rout;
bool b1, b2, b3, b4, b5, b6, b7, b8;
bool a1, a2, a3, a4;

void shift(int value, bool& var, int num)
{
    var = value & 1 << num;
}
void setup() {
 Serial.begin(9600);
  Q = Udon / N;
}

void loop() {
  sensorValue = analogRead(sensorPin);
  Uout = sensorValue * Q;
  t = Uout / 10;
  shift(t, b8, 0);
  shift(t, b7, 1);
  shift(t, b6, 2);
  shift(t, b5, 3);
  shift(t, b4, 4);
  shift(t, b3, 5);
  shift(t, b2, 6);
  shift(t, b1, 7);
  /*Искажаем 1 бит */
  b3 = !b3;
  /*We are working only with first 6 bits*/
  a1 = b3 ^ b4 ^ b6 ^ b7;
  a2 = b3 ^ b5 ^ b6 ^ b8;
  a3 = b4 ^ b5 ^ b6;
  a4 = b7 ^ b8;
  Serial.print(b8 * 1 + b7 * (1 << 1) + b6 * (1 << 2) + b5 * (1 << 3) + b4 * (1 << 4) + b3 * (1 << 5)));
  Serial.print("-");
  Serial.print(a4 * 1 + a3 * (1 << 1) + a2 * (1 << 2) + a1 * (1 << 3));
  delay(1000);
}
